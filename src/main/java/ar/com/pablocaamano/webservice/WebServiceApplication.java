package ar.com.pablocaamano.webservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebServiceApplication {
	
	// lanzamiento de la app
	public static void main(String[] args) {
		SpringApplication.run(WebServiceApplication.class, args);
	}
	
}
