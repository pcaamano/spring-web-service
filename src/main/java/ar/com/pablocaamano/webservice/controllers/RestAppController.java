package ar.com.pablocaamano.webservice.controllers;

import org.apache.commons.logging.LogFactory;
import org.apache.commons.logging.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.client.RestTemplate;

import ar.com.pablocaamano.webservice.customer.Srv;

@RestController
public class RestAppController {
	
	private static Log log = LogFactory.getLog(RestAppController.class);
	
	@Autowired
	@Qualifier("apiService")
	private Srv apiSrv; //instancia a la que apunta el component
	
	
	//Constructor
	public RestAppController(@Qualifier("apiService") Srv srv) {
		this.apiSrv = srv; // inyecta dependencias del component
	}
	
	
	// mapea e imprime html
	@RequestMapping("/api")
	public String app() {
		
		String apiResult = apiSrv.getApiData(); //guardo respuesta del Api en string
		
		log.info("weather printed ;)"); //imprime log
		
		//imprime HTML
		return "<!DOCTYPE html><html lang=\\\"es\\\" dir=\\\"ltr\\\">"
				+ "<head><meta charset=\\\"utf-8\\\"><title>API Response</title>"
				+ "<style>*{padding:0;border: 0;margin:0}"
				+ "html,body{display:flex;flex-direction:column;justify-content:center;"
				+ "align-items:center;width:100%;height: 100%;background-color: darkblue;"
				+ "color: whitesmoke;font-style: arial, helvetica, sans-serif;font-size: 14px}"
				+ "body div{width:98%;height:auto;padding20px;border:1px solid grey;background-color:white;text-align:justify;"
				+ "font-size:12px;color:#333}</style></head>"
				+ "<body><h1>API Response</h1><div>"+ apiResult
				+"</div></body></html>";

	}
	
}
