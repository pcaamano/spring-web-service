package ar.com.pablocaamano.webservice.controllers;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AppController {
	
	private static Log log = LogFactory.getLog(AppController.class);
	
	
	@RequestMapping("/")
	public String app() {
		String html = "<!DOCTYPE html><html lang=\\\"es\\\" dir=\\\"ltr\\\">"
				+ "<head><meta charset=\\\"utf-8\\\"><title>Web Service</title>"
				+ "<style>*{padding:0;border: 0;margin:0}"
				+ "html,body{display:flex;flex-direction:column;justify-content:center;"
				+ "align-items:center;width:100%;height: 100%;background-color: darkblue;"
				+ "color: whitesmoke;font-style: arial, helvetica, sans-serif;"
				+ "font-size: 14px}</style></head>"
				+ "<body><h1>Successful execution! </h1></body></html>";
		
		log.info("root OK");
		
		return html;
	}
	
}
