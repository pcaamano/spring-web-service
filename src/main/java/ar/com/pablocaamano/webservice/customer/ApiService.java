package ar.com.pablocaamano.webservice.customer;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
@Qualifier("apiService")
class ApiService implements Srv{
	
	private String city = "Buenos Aires,AR";
	
	@Override
	public String getApiData() {
		RestTemplate template = new RestTemplate();
		String today = template.getForObject("http://api.openweathermap.org/data/2.5/weather?q="+city
		+"&APPID=fb1466a4a3bbb66bf606fda5699ce6e2&mode=html", String.class);/* ingrese API aquí */
        System.out.println("Llegue hasta aquí B)"); // si ves esto en consola funciona el GET
        return today;
	}
	
}
