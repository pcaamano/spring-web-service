
## Web Service for test API

Web service to consume a API. Enter URL API in ${YOUR_PATH}\spring-web-service\src\main\java\ar\com\pablocaamano\webservice\customer\ApiService.java


# Instructions and commands

- cd into project directory
- compile:
    $ mvn clean package
- run:
    $ cd target/
    $ java -jar webDemo-0.0.1-SNAPSHOT.jar    

- for acces to root:
    localhost:1616
- for acces to API response:
    localhost:1616/api
